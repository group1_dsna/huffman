#include <Windows.h>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>

#include "Huffman.h"

using namespace std;

void main(int argc, char **argv)
{
	CHuffmanNen a;
	if (argv[1][0] == '-' && argv[1][1] == 'e')
	{
		a.Nen(argv[2], argv[3]);
	}
	else if (argv[1][0] == '-' && argv[1][1] == 'd')
	{
		if (argv[2][0] == '-' && argv[2][1] == 'a' && argv[2][2] == 'l' && argv[2][3] == 'l')
		{
			a.GiaiNen(argv[3]);
		}
	}
}