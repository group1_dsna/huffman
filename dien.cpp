﻿vector<string> get_all_files_address_within_folder(string folder)
{
	vector<string> names;
	char search_path[200];
	sprintf(search_path, "%s\\*.*", folder.c_str());
	WIN32_FIND_DATA fd; 
	string tmp1 = search_path;
	wstring tmp2 = wstring(tmp1.begin(), tmp1.end());
	LPCTSTR tmp3 = tmp2.c_str();
	HANDLE hFind = ::FindFirstFile(tmp3, &fd); 
	if(hFind != INVALID_HANDLE_VALUE) 
	{ 
		do 
		{
			if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) 
			{
				wstring tmp = fd.cFileName;
				string tmp2 = string(tmp.begin(), tmp.end());
				tmp2 = folder + "\\" + tmp2;
				names.push_back(tmp2);
			}
		} while(::FindNextFile(hFind, &fd)); 
		::FindClose(hFind); 
	} 
	return names;
}

bool CHuffmanNen::KhoiTaoDSDiaChi(string diaChi)
{
	DiaChiLuuFileNen = diaChi;
	TenThuMucNen = "";
	while (DiaChiLuuFileNen[DiaChiLuuFileNen.length() - 1] != '\\' && DiaChiLuuFileNen[DiaChiLuuFileNen.length() - 1] != ':')
	{
		TenThuMucNen = DiaChiLuuFileNen[DiaChiLuuFileNen.length() - 1] + TenThuMucNen;
		DiaChiLuuFileNen.erase(DiaChiLuuFileNen.length() - 1, 1);
	}

	m_DSDiaChiFIle = get_all_files_address_within_folder(diaChi);
	if (m_DSDiaChiFIle.size() == 0)
		return false;
	m_DSDungLuong.resize(m_DSDiaChiFIle.size());
	return true;
}

void CHuffmanNen::KhoiTaoBangThongKe(void)
{
	unsigned char x;
	for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
	{
		fstream fin(m_DSDiaChiFIle[i], ios::in | ios:: binary);
		fin.seekg(0, ios::end);
		m_DSDungLuong[i] = fin.tellg();
		fin.seekg(0, ios::beg);
		int a = '\r\n';

		for (long long j = 0; j < m_DSDungLuong[i]; j++)
		{
			fin.read((char *)&x, 1);
			m_BangThongKe[x]++;
		}
	}
}

void SapXep(vector <SDinh> &arr)
{
	for (int j = arr.size() - 1; j > 0; j--)
		for (int i = 0; i < j; i++)
			if (arr[i].soLanXuatHien < arr[i + 1].soLanXuatHien
				|| (arr[i].soLanXuatHien == arr[i + 1].soLanXuatHien && arr[i].x > arr[i + 1].x))
			{
				SDinh tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
			}
}


void CHuffmanNen::TaoBit(int viTriNode, string dayBit)
{
	if (HuffTree[viTriNode].nLeft == -1)
	{
		if (HuffTree[viTriNode].nFreq == 0)
			m_BangMaBit[HuffTree[viTriNode].c] = "";
		else
			m_BangMaBit[HuffTree[viTriNode].c] = dayBit;
	}
	else
	{
		TaoBit(HuffTree[viTriNode].nLeft, dayBit + "0");
		TaoBit(HuffTree[viTriNode].nRight, dayBit + "1");
	}	
}

void CHuffmanNen::KhoiTaoBangMaBit(void)
{
	TaoBit(510, "");
}

void CHuffmanNen::KhoiTaoBitThua_DungLuongSauNen(void)
{
	for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
	{
		fstream fin(m_DSDiaChiFIle[i], ios::in | ios::binary);

		unsigned long dungLuong = 0;
		for (int j = 0; j < m_DSDungLuong[i]; j++)
		{
			unsigned char x;
			fin.read((char *)&x, 1);

			dungLuong += m_BangMaBit[x].length();
		}
		unsigned char soBitThua = dungLuong % 8;
		dungLuong = dungLuong / 8;
		if (soBitThua > 0)
			dungLuong++;

		m_DSDungLuongSauNen[i] = dungLuong;
		m_DSSoBitThua[i] = soBitThua;
		fin.close();
	}
}
