﻿#pragma once
/////////////////////////////////////////////////
////////Cấu trúc file sau khi nén////////////////
//////// 2 bytes chữ kí//////////////////////////
//////// 4 byte số tập tin (n)///////////////////
//////// n bộ byte //////////////////////////////
////////	mảng độ dài n tên file///////////////
//////// n bộ byte///////////////////////////////
////////	mảng n tên file//////////////////////
//////// n bộ unsigned long /////////////////////******
////////	mảng n dung lượng file///////////////
//////// n bộ byte///////////////////////////////
////////	số bit thừa//////////////////////////
//////// n bộ unsigned long//////////////////////*******
////////  ds dung lượng sau nén//////////////////
//////// 256 long long///////////////////////////
////////	bảng thống kê////////////////////////
//////// n bộ unsigned long//////////////////////********
////////	mảng vị trí byte đầu tiên của file i/
//////// noi dung////////////////////////////////


#include <iostream>
#include <Windows.h>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>

using namespace std;

#define MAX_NODES 511 // 2*256 - 1

struct SDinh
{
	unsigned char x;
	int soLanXuatHien;
};

class CHuffmanNen
{
	vector <string> m_DSDiaChiFIle;
	string DiaChiLuuFileNen;
	string TenThuMucNen;
	unsigned long m_BangThongKe[256];
	vector <unsigned long> m_DSDungLuong;
	vector <unsigned long> m_DSDungLuongSauNen;
	vector <unsigned long> m_DSViTriByteDau;
	vector <unsigned char> m_DSSoBitThua;

	struct HUFFNode
	{
		unsigned char c;	// ký tự
		long nFreq;			// trọng số
		int nLeft;			// cây con trái
		int nRight;			// cây con phải
	};

	HUFFNode HuffTree[MAX_NODES];
	vector <string> m_BangMaBit;

	void TaoBit(int viTriNode, string dayBit);
	bool KhoiTaoDSDiaChi(string diaChi);
	void KhoiTaoBangThongKe(void);
	void KhoiTaoCayHuffman(void);
	void KhoiTaoBangMaBit(void);
	void KhoiTaoBitThua_DungLuongSauNen(void);
	void GhiHeader(fstream &fout);
	void GhiNoiDung(fstream &fout);
	bool DocHeader(fstream &fin);
	void DocNoiDung(fstream &fin);
	void DocNoiDung(vector <int> dsFile, fstream &fin);
	void GiaiNenFileI(int thuTuFile, unsigned long viTri, fstream &fin);
	void XuLiDayBit8(string chuoiBit, int dodai, int &viTriChuoi, int &viTriNodeHuffman, unsigned char &kiTu);
public:
	CHuffmanNen(void);
	~CHuffmanNen(void);

	void Nen(string diaChiFolder, string diaChiHuf);
	void GiaiNen(string diaChi);
	void GiaiNen(vector <int> dsFile, string diaChi);
	void XemFile(string diaChi);
};

