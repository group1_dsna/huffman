#include <Windows.h>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>

#include "Huffman.h"

using namespace std;

vector<int> lapDSFileCanGiaiNen(string chuoi)
{
	vector <int> kq;
	while (chuoi.length() > 0)
	{
		string tmp;
		while(chuoi.length() > 0 && chuoi[0] >= '0' && chuoi[0] <= '9')
		{
			tmp = tmp + chuoi[0];
			chuoi.erase(0, 1);
		}
		if (tmp != "")
		{
			int x = atoi(tmp.c_str());
			kq.push_back(x - 1);
		}
		chuoi.erase(0, 1);
	}
	return kq;
}

void main(int argc, char **argv)
{
	CHuffmanNen a;
	if (argv[1][0] == '-' && argv[1][1] == 'e')
	{
		a.Nen(argv[2], argv[3]);
	}
	else if (argv[1][0] == '-' && argv[1][1] == 'd')
	{
		if (argv[2][0] == '-' && argv[2][1] == 'a' && argv[2][2] == 'l' && argv[2][3] == 'l')
		{
			a.GiaiNen(argv[3]);
		}
		else
		{
			vector <int> dsFile = lapDSFileCanGiaiNen(argv[2]);
			a.GiaiNen(dsFile, argv[3]);
		}
	}
	else if (argv[1][0] == '-' && argv[1][1] == 'v')
	{
		a.XemFile(argv[2]);
	}
}