﻿#include "Huffman.h"


CHuffmanNen::CHuffmanNen(void)
{
	for (int i = 0; i < 256; i++)
		m_BangThongKe[i] = 0;
	m_BangMaBit.resize(256);
}


CHuffmanNen::~CHuffmanNen(void)
{
}

vector<string> get_all_files_address_within_folder(string folder)
{
	vector<string> names;
	char search_path[200];
	sprintf(search_path, "%s\\*.*", folder.c_str());
	WIN32_FIND_DATA fd;
	string tmp1 = search_path;
	wstring tmp2 = wstring(tmp1.begin(), tmp1.end());
	LPCTSTR tmp3 = tmp2.c_str();
	HANDLE hFind = ::FindFirstFile(tmp3, &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				wstring tmp = fd.cFileName;
				string tmp2 = string(tmp.begin(), tmp.end());
				tmp2 = folder + "\\" + tmp2;
				names.push_back(tmp2);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	return names;
}

bool CHuffmanNen::KhoiTaoDSDiaChi(string diaChi)
{
	DiaChiLuuFileNen = diaChi;
	TenThuMucNen = "";
	while (DiaChiLuuFileNen[DiaChiLuuFileNen.length() - 1] != '\\' && DiaChiLuuFileNen[DiaChiLuuFileNen.length() - 1] != ':')
	{
		TenThuMucNen = DiaChiLuuFileNen[DiaChiLuuFileNen.length() - 1] + TenThuMucNen;
		DiaChiLuuFileNen.erase(DiaChiLuuFileNen.length() - 1, 1);
	}

	m_DSDiaChiFIle = get_all_files_address_within_folder(diaChi);
	if (m_DSDiaChiFIle.size() == 0)
		return false;
	m_DSDungLuong.resize(m_DSDiaChiFIle.size());
	return true;
}

void CHuffmanNen::KhoiTaoBangThongKe(void)
{
	unsigned char x;
	for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
	{
		fstream fin(m_DSDiaChiFIle[i], ios::in | ios::binary);
		fin.seekg(0, ios::end);
		m_DSDungLuong[i] = fin.tellg();
		fin.seekg(0, ios::beg);
		int a = '\r\n';

		for (long long j = 0; j < m_DSDungLuong[i]; j++)
		{
			fin.read((char *)&x, 1);
			m_BangThongKe[x]++;
		}
	}
}

void SapXep(vector <SDinh> &arr)
{
	for (int j = arr.size() - 1; j > 0; j--)
	for (int i = 0; i < j; i++)
	if (arr[i].soLanXuatHien < arr[i + 1].soLanXuatHien
		|| (arr[i].soLanXuatHien == arr[i + 1].soLanXuatHien && arr[i].x > arr[i + 1].x))
	{
		SDinh tmp = arr[i];
		arr[i] = arr[i + 1];
		arr[i + 1] = tmp;
	}
}

void CHuffmanNen::KhoiTaoCayHuffman(void)
{
	// tạo bảng thống kê mới từ m_BangThongKe...
	vector <SDinh> dsDinh(256);
	for (int i = 0; i < 256; i++)
	{
		dsDinh[i].x = i;
		dsDinh[i].soLanXuatHien = m_BangThongKe[i];
	}

	for (int i = 0; i < 256; i++)
	{
		HuffTree[i].c = i;
		HuffTree[i].nFreq = m_BangThongKe[i];
		HuffTree[i].nLeft = -1;
		HuffTree[i].nRight = -1;
	}

	for (int i = 256; i < MAX_NODES; i++)
	{
		SapXep(dsDinh);

		HuffTree[i].c = dsDinh[dsDinh.size() - 1].x;
		if (dsDinh[dsDinh.size() - 2].x < HuffTree[i].c)
			HuffTree[i].c = dsDinh[dsDinh.size() - 2].x;

		if (dsDinh[dsDinh.size() - 1].soLanXuatHien < dsDinh[dsDinh.size() - 2].soLanXuatHien
			|| (dsDinh[dsDinh.size() - 1].soLanXuatHien == dsDinh[dsDinh.size() - 2].soLanXuatHien && dsDinh[dsDinh.size() - 1].x < dsDinh[dsDinh.size() - 2].x))
		{
			int k;
			for (k = i - 1; k > -1; k--)
			{
				if (HuffTree[k].c == dsDinh[dsDinh.size() - 1].x)
					break;
			}
			HuffTree[i].nLeft = k;

			for (k = i - 1; k > -1; k--)
			{
				if (HuffTree[k].c == dsDinh[dsDinh.size() - 2].x)
					break;
			}
			HuffTree[i].nRight = k;
		}
		else
		{
			int k;
			for (k = i - 1; k > -1; k--)
			{
				if (HuffTree[k].c == dsDinh[dsDinh.size() - 1].x)
					break;
			}
			HuffTree[i].nRight = k;

			for (k = i - 1; k > -1; k--)
			{
				if (HuffTree[k].c == dsDinh[dsDinh.size() - 2].x)
					break;
			}
			HuffTree[i].nLeft = k;
		}

		HuffTree[i].nFreq = HuffTree[HuffTree[i].nLeft].nFreq + HuffTree[HuffTree[i].nRight].nFreq;

		dsDinh[dsDinh.size() - 2].x = HuffTree[i].c;
		dsDinh[dsDinh.size() - 2].soLanXuatHien = dsDinh[dsDinh.size() - 2].soLanXuatHien + dsDinh[dsDinh.size() - 1].soLanXuatHien;
		dsDinh.erase(dsDinh.begin() + dsDinh.size() - 1);

	}
}

void CHuffmanNen::TaoBit(int viTriNode, string dayBit)
{
	if (HuffTree[viTriNode].nLeft == -1)
	{
		if (HuffTree[viTriNode].nFreq == 0)
			m_BangMaBit[HuffTree[viTriNode].c] = "";
		else
			m_BangMaBit[HuffTree[viTriNode].c] = dayBit;
	}
	else
	{
		TaoBit(HuffTree[viTriNode].nLeft, dayBit + "0");
		TaoBit(HuffTree[viTriNode].nRight, dayBit + "1");
	}
}

void CHuffmanNen::KhoiTaoBangMaBit(void)
{
	TaoBit(510, "");
}

void CHuffmanNen::KhoiTaoBitThua_DungLuongSauNen(void)
{
	for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
	{
		fstream fin(m_DSDiaChiFIle[i], ios::in | ios::binary);

		unsigned long dungLuong = 0;
		for (int j = 0; j < m_DSDungLuong[i]; j++)
		{
			unsigned char x;
			fin.read((char *)&x, 1);

			dungLuong += m_BangMaBit[x].length();
		}
		unsigned char soBitThua = dungLuong % 8;
		dungLuong = dungLuong / 8;
		if (soBitThua > 0)
			dungLuong++;

		m_DSDungLuongSauNen[i] = dungLuong;
		m_DSSoBitThua[i] = soBitThua;
		fin.close();
	}
}

void CHuffmanNen::GhiHeader(fstream &fout)
{
	string diaChi = DiaChiLuuFileNen + TenThuMucNen + ".huf";
	char chuKi[] = "G1";


	fout.write(chuKi, 2);


	int soFile = m_DSDiaChiFIle.size();
	fout.write((char *)&soFile, sizeof(soFile));

	long long conTro = 6;

	// ghi do dai dia chi
	for (int i = 0; i < soFile; i++)
	{
		unsigned char doDai = m_DSDiaChiFIle[i].length();
		fout.write((char *)&doDai, sizeof(doDai));
		conTro = conTro + sizeof(doDai);
	}

	// ghi dia chi
	for (int i = 0; i < soFile; i++)
	{
		fout.write(m_DSDiaChiFIle[i].c_str(), m_DSDiaChiFIle[i].length());
		conTro = conTro + m_DSDiaChiFIle[i].length();
	}

	// ghi dung luong truoc khi nen
	for (int i = 0; i < soFile; i++)
	{
		unsigned long tmp = m_DSDungLuong[i];
		fout.write((char *)&tmp, sizeof(tmp));
		conTro = conTro + sizeof(tmp);
	}

	// ghi so bit thua
	for (int i = 0; i < soFile; i++)
	{
		unsigned char soBitThua = m_DSSoBitThua[i];
		fout.write((char *)&soBitThua, sizeof(soBitThua));
		conTro = conTro + sizeof(soBitThua);
	}

	// ghi dung luong sau khi nen
	for (int i = 0; i < soFile; i++)
	{
		unsigned long tmp = m_DSDungLuongSauNen[i];
		fout.write((char *)&tmp, sizeof(tmp));
		conTro = conTro + sizeof(tmp);
	}

	// ghi bang thong ke
	for (int i = 0; i < 256; i++)
	{
		unsigned long tmp = m_BangThongKe[i];
		fout.write((char *)&tmp, sizeof(tmp));
		conTro = conTro + sizeof(tmp);
	}

	// ghi con tro file
	for (int i = 0; i < soFile; i++)
	{
		unsigned long tmp = conTro + sizeof(tmp)* soFile;
		fout.write((char *)&tmp, sizeof(tmp));
		m_DSViTriByteDau[i] = tmp;
		conTro += m_DSDungLuongSauNen[i];
	}
}

unsigned char ChuyenString8SangChar(string s)
{
	unsigned char kq = 0;
	for (int i = 0; i < 8; i++)
	{
		kq = kq * 2 + s[i] - 48;
	}
	return kq;
}

void CHuffmanNen::GhiNoiDung(fstream &fout)
{
	m_DSDungLuongSauNen.resize(m_DSDiaChiFIle.size());
	m_DSSoBitThua.resize(m_DSDiaChiFIle.size());
	for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
	{
		cout << "Nen file: " << m_DSDiaChiFIle[i] << "... ";
		fstream fin(m_DSDiaChiFIle[i], ios::in | ios::binary);
		string tmp = "";
		for (int dungLuong = 0; dungLuong < m_DSDungLuong[i]; dungLuong++)
		{
			unsigned char x;
			fin.read((char *)&x, 1);

			tmp = tmp + m_BangMaBit[x];
			while (tmp.length() >= 8)
			{
				string tmp2 = tmp.substr(0, 8);
				tmp.erase(0, 8);
				x = ChuyenString8SangChar(tmp2);
				fout.write((char *)&x, sizeof(x));
			}
		}
		if (tmp != "")
		{
			unsigned char x;
			while (tmp.length() < 8)
				tmp = tmp + "0";
			x = ChuyenString8SangChar(tmp);
			fout.write((char *)&x, sizeof(x));
		}
		fin.close();
		cout << "Xong" << endl;
	}
}

void CHuffmanNen::Nen(string diaChiFolder, string diaChiHuf)
{
	KhoiTaoDSDiaChi(diaChiFolder);
	m_DSViTriByteDau.resize(m_DSDiaChiFIle.size());

	cout << "Lap bang thong ke... ";
	KhoiTaoBangThongKe();
	cout << "xong." << endl;

	cout << "Tao cay Huffman... ";
	KhoiTaoCayHuffman();
	cout << "xong." << endl;

	cout << "Tao bang ma bit... ";
	KhoiTaoBangMaBit();
	cout << "xong." << endl;

	m_DSDungLuongSauNen.resize(m_DSDiaChiFIle.size());
	m_DSSoBitThua.resize(m_DSDiaChiFIle.size());
	KhoiTaoBitThua_DungLuongSauNen();

	fstream fout(diaChiHuf, ios::out | ios::binary);

	GhiHeader(fout);
	GhiNoiDung(fout);

	fout.close();
	cout << "Hoan tat" << endl;
}

bool CHuffmanNen::DocHeader(fstream &fin)
{
	unsigned char x[2];
	fin.read((char *)&x, 2);
	if (x[0] != 'G' || x[1] != '1')
		return false;

	int n;
	fin.read((char *)&n, sizeof(n));

	vector <unsigned char> arr_DoDai(n);
	for (int i = 0; i < n; i++)
	{
		unsigned char tmp;
		fin.read((char *)&tmp, sizeof(tmp));
		arr_DoDai[i] = tmp;
	}

	m_DSDiaChiFIle.resize(n);
	char *tmp = new char[256];
	for (int i = 0; i < n; i++)
	{
		fin.read(tmp, arr_DoDai[i]);
		tmp[arr_DoDai[i]] = '\0';
		m_DSDiaChiFIle[i] = tmp;
	}
	delete[] tmp;

	m_DSDungLuong.resize(n);
	for (int i = 0; i < n; i++)
	{
		unsigned long tmp;
		fin.read((char *)&tmp, sizeof(tmp));
		m_DSDungLuong[i] = tmp;
	}

	// doc so bit thua
	m_DSSoBitThua.resize(n);
	for (int i = 0; i < n; i++)
	{
		unsigned char soBitThua;
		fin.read((char *)&soBitThua, sizeof(soBitThua));
		m_DSSoBitThua[i] = soBitThua;
	}

	// doc dung luong sau khi nen
	m_DSDungLuongSauNen.resize(n);
	for (int i = 0; i < n; i++)
	{
		unsigned long tmp;
		fin.read((char *)&tmp, sizeof(tmp));
		m_DSDungLuongSauNen[i] = tmp;
	}

	for (int i = 0; i < 256; i++)
	{
		fin.read((char *)&m_BangThongKe[i], sizeof(m_BangThongKe[0]));
	}

	m_DSViTriByteDau.resize(n);
	for (int i = 0; i < n; i++)
	{
		fin.read((char *)&m_DSViTriByteDau[i], sizeof(m_DSViTriByteDau[0]));
	}
	return true;
}

void ChuyenCharSangString8(unsigned char c, string &string8)
{
	for (int i = 0; i < 8; i++)
	{
		unsigned char tmp = c % 2;
		string8[7 - i] = tmp + 48;
		c = c / 2;
	}
}

void CHuffmanNen::XuLiDayBit8(string chuoiBit, int dodai, int &viTriChuoi, int &viTriNodeHuffman, unsigned char &kiTu)
{
	if (HuffTree[viTriNodeHuffman].nLeft == -1)
	{
		kiTu = HuffTree[viTriNodeHuffman].c;
		return;
	}
	if (viTriChuoi == dodai || chuoiBit[0] == ' ')
	{
		viTriChuoi = -1;
		return;
	}

	if (chuoiBit[viTriChuoi] == '0')
	{
		viTriNodeHuffman = HuffTree[viTriNodeHuffman].nLeft;
		viTriChuoi++;
		XuLiDayBit8(chuoiBit, dodai, viTriChuoi, viTriNodeHuffman, kiTu);
	}
	else
	{
		viTriNodeHuffman = HuffTree[viTriNodeHuffman].nRight;
		viTriChuoi++;
		XuLiDayBit8(chuoiBit, dodai, viTriChuoi, viTriNodeHuffman, kiTu);
	}
}

void congChuoi(string &chuoi, int &doDai, string &string8)
{
	for (int i = 0; i < 8; i++)
	{
		chuoi[i + doDai] = string8[i];
	}
	doDai += 8;
}

void xoaChuoi(string &chuoi, int &doDai, int &soKiTuDau)
{
	for (int i = 0; i < doDai; i++)
	{
		chuoi[i] = chuoi[i + soKiTuDau];
	}
	doDai -= soKiTuDau;
}

void CHuffmanNen::GiaiNenFileI(int thuTuFile, unsigned long viTri, fstream &fin)
{
	string tenFile;
	for (int i = m_DSDiaChiFIle[thuTuFile].length() - 1; i > -1; i--)
	{
		if (m_DSDiaChiFIle[thuTuFile][i] == '\\')
			break;
		tenFile = m_DSDiaChiFIle[thuTuFile][i] + tenFile;
	}

	fin.seekg(viTri, ios::beg);

	string diaChi = TenThuMucNen + "//" + tenFile;
	cout << diaChi << "... ";

	fstream fout(diaChi, ios::out | ios::binary);
	string chuoiBit = "                              ";		// length = 30
	string string8 = "        ";							// length = 8
	int doDai = 0;
	unsigned char x;
	for (int i = 0; i < m_DSDungLuongSauNen[thuTuFile]; i++)
	{
		fin.read((char *)&x, sizeof(x));
		ChuyenCharSangString8(x, string8);

		congChuoi(chuoiBit, doDai, string8);

		if (i == m_DSDungLuongSauNen[thuTuFile] - 1 && m_DSSoBitThua[thuTuFile] != 0)
		for (int k = 0; k < 8 - m_DSSoBitThua[thuTuFile]; k++)
		{
			doDai--;
		}
		do
		{
			int viTriChuoi = 0;
			int viTriHuffMan = 510;
			XuLiDayBit8(chuoiBit, doDai, viTriChuoi, viTriHuffMan, x);
			if (viTriChuoi == -1)
				break;
			xoaChuoi(chuoiBit, doDai, viTriChuoi);
			fout.write((char *)&x, 1);
		} while (doDai > 0);
	}
	fout.close();
}

void CHuffmanNen::DocNoiDung(fstream &fin)
{
	for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
	{
		cout << "Giai nen file: ";
		GiaiNenFileI(i, m_DSViTriByteDau[i], fin);

		cout << "Xong" << endl;
	}
}

void CHuffmanNen::GiaiNen(string diaChi)
{
	fstream fin(diaChi, ios::in | ios::binary);
	cout << "Doc Header...";
	if (!DocHeader(fin))
		cout << "Khong ho tro kieu file";
	else
	{
		cout << "Xong" << endl;
		for (int i = 0; i < diaChi.length(); i++)
		{
			if (diaChi[i] == '\\')
				break;
			TenThuMucNen = TenThuMucNen + diaChi[i];
		}
		cout << "Khoi tao cay Huffman...";
		KhoiTaoCayHuffman();
		cout << "Xong" << endl;

		cout << "Khoi tao bang ma bit...";
		KhoiTaoBangMaBit();
		cout << "Xong" << endl;

		DocNoiDung(fin);
	}
}

void CHuffmanNen::DocNoiDung(vector <int> dsFile, fstream &fin)
{
	for (int i = 0; i < dsFile.size(); i++)
	{
		cout << "Giai nen file " << dsFile[i] + 1 << ": ";
		if (dsFile[i] > m_DSDiaChiFIle.size() - 1)
			cout << "File " << dsFile[i] + 1 << " khong ton tai...";
		else
			GiaiNenFileI(dsFile[i], m_DSViTriByteDau[dsFile[i]], fin);
		cout << "Xong" << endl;
	}
}

void CHuffmanNen::GiaiNen(vector <int> dsFile, string diaChi)
{
	fstream fin(diaChi, ios::in | ios::binary);
	cout << "Doc Header...";
	if (!DocHeader(fin))
		cout << "Khong ho tro kieu file";
	else
	{
		cout << "Xong" << endl;
		for (int i = 0; i < diaChi.length(); i++)
		{
			if (diaChi[i] == '\\')
				break;
			TenThuMucNen = TenThuMucNen + diaChi[i];
		}
		cout << "Khoi tao cay Huffman...";
		KhoiTaoCayHuffman();
		cout << "Xong" << endl;

		cout << "Khoi tao bang ma bit...";
		KhoiTaoBangMaBit();
		cout << "Xong" << endl;

		DocNoiDung(dsFile, fin);
	}
}

void CHuffmanNen::XemFile(string diaChi)
{
	fstream fin(diaChi, ios::in | ios::binary);
	cout << "Doc Header...";
	if (!DocHeader(fin))
		cout << "Khong ho tro kieu file";
	else
	{
		cout << "Xong" << endl;
		for (int i = 0; i < diaChi.length(); i++)
		{
			if (diaChi[i] == '\\')
				break;
			TenThuMucNen = TenThuMucNen + diaChi[i];
		}

		cout << "   Ten file            " << "Dung luong khi nen       " << "Dung luong sau nen" << endl;

		for (int i = 0; i < m_DSDiaChiFIle.size(); i++)
		{
			string tmp = m_DSDiaChiFIle[i];
			int viTri = tmp.length() - 1;
			while (tmp[viTri] != '\\' && tmp[viTri] != ':')
			{
				viTri--;
			}
			tmp.erase(0, viTri + 1);
			cout << "1. " << setw(20) << left << tmp << setw(10) << right << m_DSDungLuong[i] << setw(15) << left << " byte(s) " << setw(10) << right << m_DSDungLuongSauNen[i] << " byte(s)" << endl;
		}
	}
}